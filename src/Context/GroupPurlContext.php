<?php

namespace Drupal\group_purl\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\purl\Event\ModifierMatchedEvent;
use Drupal\purl\PurlEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class GroupPurlContext.
 */
class GroupPurlContext implements ContextProviderInterface, EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The modifier matched event.
   *
   * @var \Drupal\purl\Event\ModifierMatchedEvent|null
   */
  protected $modifierMatched;

  /**
   * Contexts collected from events.
   *
   * @var array
   */
  protected $contexts = [];

  /**
   * Constructs a new GroupPurlContext object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $currentRouteMatch) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[PurlEvents::MODIFIER_MATCHED] = ['onModifierMatched'];
    return $events;
  }

  /**
   * This method is called whenever the purl.modifier_matched event is
   * dispatched.
   *
   * @param \Drupal\purl\Event\ModifierMatchedEvent $event
   *   The event object.
   */
  public function onModifierMatched(ModifierMatchedEvent $event) {
    $this->modifierMatched = $event;
    $this->contexts[$event->getMethod()->getId()] = $event->getModifier();
    ksort($this->contexts);
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    $cacheability = new CacheableMetadata();
    $cacheability->setCacheContexts(['purl']);

    $group = $this->getGroupFromRoute();
    if ($group) {
      $context = EntityContext::fromEntity($group);
      $context->addCacheableDependency($cacheability);
      return ['group' => $context];
    }
    return [];
  }

  /**
   * Retrieves the active group from the route or modifier.
   *
   * @return \Drupal\group\Entity\Group|null
   *   The active group entity, or NULL if none is found.
   */
  public function getGroupFromRoute() {
    if ($this->modifierMatched !== NULL) {
      $storage = $this->entityTypeManager->getStorage('group');
      $group = $storage->load($this->modifierMatched->getValue());
      return $group;
    }

    $routename = $this->currentRouteMatch->getRouteName();
    if ($routename !== null && strpos($routename, 'entity.group.') === 0) {
      $group = $this->currentRouteMatch->getParameter('group');
      return $group;
    }

    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $context = EntityContext::fromEntityTypeId('group', $this->t('Group from Purl'));
    return ['group' => $context];
  }

}
