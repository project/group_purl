<?php

namespace Drupal\group_purl\Plugin\Purl\Provider;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\purl\Plugin\Purl\Provider\ProviderAbstract;
use Drupal\purl\Plugin\Purl\Provider\ProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @PurlProvider(
 *   id = "group_purl_provider",
 *   title = @Translation("A provider pair for Group module.")
 * )
 */
class GroupPurlProvider extends ProviderAbstract implements ProviderInterface, ContainerFactoryPluginInterface {

  protected $storage;
  /**
   * Cache id for the group purl modifiers.
   */
  const GROUP_PURL_MODIFIERS_CACHE_KEY = 'group_purl:group_purl_modifiers';

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $manager;

  /**
   * AliasManger definition.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('cache.default'),
      $container->get('entity_type.manager'),
      $container->get('path_alias.manager'),
      $container->get('database'),
    );
  }

  /**
   * GroupDomainPurlProvider constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   Entity type manager service.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   Alias Manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   *
   */
  public function __construct(CacheBackendInterface $cache, EntityTypeManagerInterface $manager, AliasManagerInterface $aliasManager, Connection $connection) {
    $this->cache = $cache;
    $this->manager = $manager;
    $this->aliasManager = $aliasManager;
    $this->connection = $connection;
  }

  /**
   * @inheritDoc
   */
  public function getModifierData() {

    if ($exists = $this->cache->get(self::GROUP_PURL_MODIFIERS_CACHE_KEY)) {
      return $exists->data;
    }

    $modifier_paths = $this->connection->select('path_alias', 'pa')
      ->fields('pa', ['alias', 'path'])
      ->condition('path', '/group/%', 'LIKE')
      ->condition('path', '/group/%/%', 'NOT LIKE')
      ->distinct()
      ->execute()
      ->fetchAllAssoc('path');

    $modifiers = [];
    foreach ($modifier_paths as $modifier_path) {
      $source_gid = str_replace('/group/', '', $modifier_path->path);

      if (is_numeric($source_gid)) {
        $path = substr($modifier_path->alias, 1);
        $modifiers[$path] = $source_gid;
      }
    }

    if ($modifiers) {
      $this->cache->set(self::GROUP_PURL_MODIFIERS_CACHE_KEY, $modifiers, Cache::PERMANENT, [self::GROUP_PURL_MODIFIERS_CACHE_KEY]);
    }

    return $modifiers;
  }

  public function getModifierDataById($id) {

    if ($exists = $this->cache->get(self::GROUP_PURL_MODIFIERS_CACHE_KEY)) {
      $exists_data = array_flip($exists->data);

      if (isset($exists_data[$id])) {
        return [$exists_data[$id] => $id];
      }
    }

    /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
    $group = $this->manager->getStorage('group')->load($id);
    $modifiers = [];

    if (!$group) {
      return $modifiers;
    }

    $path = $this->aliasManager->getAliasByPath('/group/' . $id);
    $path = substr($path, 1);
    $modifiers[$path] = $id;

    return $modifiers;
  }

  public function getModifierDataByKey($value) {

    if ($exists = $this->cache->get(self::GROUP_PURL_MODIFIERS_CACHE_KEY)) {
      $exists_data = $exists->data;
      if (isset($exists_data[$value])) {
        return [$value => $exists_data[$value]];
      }
    }

    /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
    $storage = $this->manager->getStorage('group');
    $path = $this->aliasManager->getPathByAlias("/$value");
    $is_group_source = preg_match('/^\/group\/(.*)/', $path, $matches);
    $modifiers = [];
    if ($is_group_source && $storage->load($matches[1])) {
      $modifiers[$value] = $matches[1];
    }
    return $modifiers;
  }

}
